/**
 * Created by benji on 25/10/2017.
 */


//Reducer global regroupant selectedReducer et updateModelReducer
import { combineReducers } from 'redux';
import selectedReducer from "./selectedReducer";

const globalReducer = combineReducers({
    selectedReducer: selectedReducer,
});
export default globalReducer;