/**
 * Created by benji on 25/10/2017.
 */


//Reducer mettant à jour les éléments sélectionnés
const selectedReducer = (state = {slid: {}, content_dragged : null}, action) => {
    console.log(action);
    switch (action.type) {
        case 'UPDATE_SELECTED_SLID':
            return {slid: action.obj,  content_dragged: state.content_dragged};
        case 'UPDATE_DRAGGED_ELT':
            console.log("Id du true " + action.obj);
            return {slid : state.slid, content_dragged: action.obj};

        default:
            return state;
    }
};
export default selectedReducer;