/**
 * Created by benji on 25/10/2017.
 */


//Liste des actions permettant d’interagir avec le store
export const setSelectedSlid=(slid_obj)=>{
    return {
        type: 'UPDATE_SELECTED_SLID',
        obj:slid_obj
    };
};