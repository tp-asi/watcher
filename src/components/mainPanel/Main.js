/**
 * Created by benji on 25/10/2017.
 */
import React from "react";
import "./main.css";
import "../../lib/bootstrap.min.css";
import {Provider} from "react-redux";
import {createStore} from "redux";
import globalReducer from "../../reducers/index";
import {setSelectedSlid} from "../../actions/index";
import Comm from "../../services/Comm";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import Slid from "../common/slid/containers/Slid";


// var comm = require('Comm');
const store = createStore(globalReducer);

export default class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            snackbar: {
                open: false,
                text: ""
            },
            pres: null
        };

        this.comm = new Comm();

        this.getPres();
        this.subscribeToSlidEvent();
    }

    subscribeToSlidEvent() {
        this.comm.socketConnection(this.onSlidEvent);
    }

    onSlidEvent(slid) {
        store.dispatch(setSelectedSlid(slid));
    }

    getPres() {
        let id = "efa0a79a-2f20-4e97-b0b7-71f824bfe349";

        this.comm.loadPres(id,
            pres => this.getPresSuccess(pres),
            error => this.getPresError(error)
        );
    }

    getPresSuccess(pres) {
        this.setState({
            pres: pres
        });
    }

    getPresError(error) {
        this.setState({
            snackbar: {
                open: true,
                text: "Error while loading presentation"
            }
        });
    }

    getContentMapError(error) {
        this.setState({
            snackbar: {
                open: true,
                text: "Error while loading content map"
            }
        });
    }


    render() {
        let presContent;
        if (this.state.pres) {
            presContent = <div>
                <h3>{this.state.pres.title}</h3>
                <p>{this.state.pres.description}</p>
            </div>
        }

        return (
            <MuiThemeProvider>
                <Provider store={store}>
                    <div>
                        {presContent}

                        <div className='container-fluid height-100'>
                            <Slid />
                        </div>
                    </div>
                </Provider>
            </MuiThemeProvider>
        );
    }
}
