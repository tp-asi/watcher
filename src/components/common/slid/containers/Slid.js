/**
 * Created by benji on 25/10/2017.
 */
//Composant représentant un Slid (meta data et contenu)
import React from "react";
import Content from "../../../common/content/containers/Content";
import EditMetaSlid from "../components/EditMetaSlid";
import {setSelectedSlid} from "../../../../actions/index";
import {connect} from "react-redux";

class Slid extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        let slid = this.props.slid;
        let contentHTML;
        if(slid === null){
            contentHTML = <p>No slid</p>
        }
        else if(slid.content){
            contentHTML = <Content id={slid.content.id} src={slid.content.src} type={slid.content.type}
                                   title={slid.content.title} onlyContent={false}/> ;
        }
        else{
            contentHTML = <p>No content</p>
        }

        return (
            <div className="border">
                <h3>{slid.title}</h3>
                <p>{slid.txt}</p>
                {contentHTML}
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        slid: state.selectedReducer.slid,
    }
};


export default connect(mapStateToProps)(Slid);