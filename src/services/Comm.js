/**
 * Created by benji on 20/11/2017.
 */

let io = require('socket.io-client');
let axios = require('axios');
const Tools = require('../services/Tool.js');


export default class Comm {
    constructor() {
        this.comm = {};
        this.comm.io = {};
        this.socket = "";
        this.emitOnConnect = this.emitOnConnect.bind(this);

    }

    toString() {
        return '';
    }

    loadPres(presId, callback, callbackErr) {
        console.log("Load prez");
        axios.get('/loadPres')
            .then(function (data) {
                let size = Object.keys(data.data).length;
                let loadedPres = ""
                if (size > 0) {
                    loadedPres = data.data[Object.keys(data.data)[0]];
                }
                callback(loadedPres);
            })
            .catch(function (error) {
                callbackErr(error);
            });
    }

    emitOnConnect(message) {
        console.log("message");
        console.log("socket");
        console.log(this.socket);
        console.log("this.comm.io.uuid");
        console.log(this.comm.io.uuid);
        this.socket.emit('data_comm', {'id': this.comm.io.uuid}, function (test) {
            console.log(test);
        });
    }

    socketConnection(onSlidEvent) {
        this.socket = io();
        this.comm.io.uuid = Tools.generateUUID();
        this.socket.on('connection', message => {
            this.emitOnConnect(message)
        });

        // this.socket.on('newPres', function (socket) {
        //
        // });
        this.socket.on('slidEvent',
            slid => this.onSlideEv(slid, onSlidEvent)
        );
    }

    onSlideEv(slid, func){
        console.log("Recu Slid");
        console.log(slid);
        func(slid);
    }
}



